# Copyright 2022 Ricardo Yarza

# This file is part of rytools.

# rytools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# rytools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with rytools.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import scipy.integrate
import scipy.interpolate
import scipy.optimize
import rytools.units
import os, contextlib

cgs = rytools.units.get_cgs()

def solvePolytrope(n):
    def LERHS(xi, coords, n):
        theta, y = coords
        return [y, - ( theta ** n ) - 2 * y / xi ]

    sol = scipy.integrate.solve_ivp(LERHS, [1e-16, 7], [1, 0], rtol = 2.221e-14, args = (n,), dense_output = True)

    def trunc_sol(theta):
        if sol.sol(theta)[0] >1: return -1
        else: return sol.sol(theta)[0]
    
    try:
        with open(os.devnull, 'w') as devnull:
            with contextlib.redirect_stdout(devnull):
                xi1 = scipy.optimize.brentq(trunc_sol, 1, 7, rtol = 1e-16, atol = 1e-16, maxiter = 1000000)
    except:
        xi1 = scipy.optimize.newton(lambda x: sol.sol(x)[0], 3, rtol = 1e-17, maxiter = 1000000)

    return sol.sol, xi1

class Polytrope:
    def __init__(self, n):
        self.n = n
        self._sol, self._xi1 = solvePolytrope(n)

    def theta(self, xi):
        return self._sol(xi)[0]

    def dthetadxi(self, xi):
        return self._sol(xi)[1]

    @property
    def bindingEnergy(self):
        return 3 / ( self.n - 5 )

    @property
    def radius(self):
        return self._xi1
        
    def rho(self, xi):
        return self._sol(xi)[0] ** self.n

    def P(self, xi):
        return self.rho(xi) ** ( 1 + 1 / self.n )
        
    def drhodxi(self, xi):
        return self.n * ( self.theta(xi) ** ( self.n - 1 ) ) * self.dthetadxi(xi)

class DimensionalPolytrope:
    def __init__(self, n, m, r):
        self.m = m
        self.r = r
        self.n = n
        self._sol, self._xi1 = solvePolytrope(n)
        self._dthdxi1 = self._sol(self._xi1)[1]

#       Commpute the necessary constants to go from dimensionless to dimensional solution
        self._rhoc  = - ( self._xi1/3/self._dthdxi1 ) * 3 * self.m / 4 / np.pi / (self.r**3)
        self._pc    = cgs["GNEWT"] * self.m * self.m / (self.r ** 4) / ( 4*np.pi*(self.n+1) * (self._dthdxi1**2)  )
        self._k     = self._pc / ( self._rhoc ** ( 1 + 1 / n ) )
        self._alpha = np.sqrt( ( self.n + 1 ) * self._k / 4 / np.pi / cgs["GNEWT"]) * ( self._rhoc ** ( (1-self.n  )/2/self.n ) )

#   Gives the value of the polytropic solution as a function of the dimensionless radius
    def theta(self, xi):
        return self._sol(xi)[0]

    def dthdxi(self, xi):
        return self._sol(xi)[1]

#   Density in CGS as a function of radius in CGS
    def rho(self, r):
        xi = r / self._alpha
        return self._rhoc * ( self.theta(xi) ** self.n )

    @property
    def binding_energy(self):
      return 3 * cgs['GNEWT'] * self.m * self.m / ( self.n - 5 ) / self.r

#   Pressure in CGS as a function of radius in CGS
    def p(self, r):
        xi = r / self._alpha
        return self._pc * ( self.theta(xi) ** ( self.n + 1 ) )
