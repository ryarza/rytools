# Copyright 2022 Ricardo Yarza

# This file is part of rytools.

# rytools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# rytools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with rytools.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import scipy.interpolate
import rytools

def load_mesa_profile(profile_path):

    import mesa_reader
    prof = mesa_reader.MesaData(profile_path)
    cgs = rytools.units.get_cgs()

    p = {}
    p['n'] = len(prof.rmid)
    p['r'] = np.flip(prof.rmid) * cgs['RSUN']
    p['menc'] = np.flip(prof.mmid) * cgs['MSUN']
    p['mext'] = p['menc'][-1] - p['menc']
    p['rho'] = np.flip(prof.Rho)
    p['L'] = np.flip(prof.luminosity) * cgs['LSUN']
    p['cs'] = np.flip(prof.csound)
    p['dr'] = np.flip(prof.dr)
    p['h_rho'] = - p['rho'] / np.gradient(p['rho'], p['r'], edge_order = 2)
    p['tau_lc'] = ( p['r'][-1] - p['r'] ) / cgs['CL']

    try:
        p['v_conv'] = np.flip(prof.conv_vel)
        p['conv_zone_edge'] = np.where(p['v_conv'] > 0)[0][0]
        p['t_thermal'] = np.flip(prof.thermal_time_to_surface)
        p['t_conv'] = tau_convection(p['r'], p['v_conv'])
        p['t_kh'] = tau_energy_transport(p['r'], p['dr'], p['t_thermal'], p['v_conv'])
    except:
        pass


    try:
        p['x_h1'] = np.flip(prof.x_mass_fraction_H)
    except:
        pass


    p['itp'] = {}
    for key in ['menc', 'rho', 'cs', 'h_rho', 't_kh']:
        p['itp'][key] = scipy.interpolate.interp1d(p['r'], p[key], kind = 'linear')

    return p

def mesa_profile_to_hdf5(profile_path, output_path = None):

    import h5py
    import os
    p = load_mesa_profile(profile_path)

    if output_path == None:
        output_path = os.path.splitext(profile_path)[0]+'.h5'

    f = h5py.File(output_path, 'w')
    for field in ['r', 'rho', 'cs', 'menc']:
        f.create_dataset(field, data = p[field])

    f.create_dataset('hrho', data = p['h_rho'])
    f.create_dataset('drhodr', data = np.gradient(p['rho'], p['r'], edge_order = 2))

    f.attrs['ncells'] = len(p['r'])

    f.close()

def tau_convection(r, v_conv):
    t_conv = np.empty_like(r)
    conv_region = np.where(v_conv > 0)
    non_conv_region = np.where(v_conv <= 0)
    t_conv[conv_region] = ( r[-1] - r[conv_region] ) / v_conv[conv_region]
    t_conv[non_conv_region] = np.inf
    return t_conv

def tau_energy_transport(r, dr, t_thermal, v_conv):

    assert r[0] < r[-1], "Profile must start at core and end at surface!"

    t_rd = - np.diff(t_thermal)
    t_rd = np.append(t_rd, t_thermal[-1])

    # Make sure that the per-cell radiative diffusion times add up to the total one!
    assert np.isclose(np.sum(t_rd), t_thermal[0], rtol = 1.e-10), "%.15e %.15e" % ( np.sum(t_rd), t_thermal[0] )

    non_conv_region = np.where(v_conv <= 0)
    conv_region = np.where(v_conv > 0)
    t_conv = np.empty_like(r)
    t_conv[conv_region] = dr[conv_region] / v_conv[conv_region]
    t_conv[non_conv_region] = np.inf

    t_transport_per_cell = np.minimum(t_conv, t_rd)
    assert (t_transport_per_cell > 0).all()

    t_kh = np.empty_like(r)
    for i in range(len(r)):
        t_kh[i] = np.sum(t_transport_per_cell[i:])

    return t_kh

def enclosed_mass_profile(r, rho):

    """Computes the enclosed mass profile of a sphere given its density profile.
  
    :param r: Radial coordinate values at which other inputs are specified.
    :type r: `numpy.ndarray`
  
    :param rho: Mass density.
    :type rho: `numpy.ndarray`
  
    :return: Total mass.
    :rtype: `float`
    """

    assert ( np.diff(r) > 0 ).all(), "Radius must be increasing."
    return rytools.integral_array(x = r, y = 4 * np.pi * r * r * rho)

def binding_energy(r, rho, menc = None, idx0 = 0, idx1 = -1, G = None):

    """Computes binding energy for a self-gravitating sphere given a density profile.
  
    :param r: Radial coordinate values at which other inputs are specified.
    :type r: `numpy.ndarray`
  
    :param rho: Mass density.
    :type rho: `numpy.ndarray`
  
    :param menc: Enclosed mass. If not given, it'll be computed from density.
    :type menc: `numpy.ndarray, optional`

    :param idxo: Index for lower limit of integration.
    :type idx0: `int, optional`

    :param idx1: Index for upper limit of integration.
    :type idx1: `int, optional`

    :param G: Value for the gravitational constant. If not provided, CGS value is assumed.
    :type G: `float, optional`
  
    :return: Binding energy.
    :rtype: `float`

    """

  # Get constants (for G)
    if G == None: G = rytools.units.get_cgs()['G']

  # Get mass enclosed as a function of radius, if needed
    if type(menc) != np.ndarray: menc = rytools.stars.enclosed_mass_profile(r, rho)

  # Resize arrays to limits of integration
    if idx1 == -1:
      r = r[idx0:]
      rho = rho[idx0:]
      menc = menc[idx0:]
    else:
      r = r[idx0:idx1+1]
      rho = rho[idx0:idx1+1]
      menc = menc[idx0:idx1+1]

    return - 4 * np.pi * G * scipy.integrate.simpson(menc * rho * r, x = r)

def binding_energy_outer(r, rho, menc = None, G = None):

    """Computes binding energy for material outside a given radius, as a function of radius.
  
    :param r: Radial coordinate values at which other inputs are specified.
    :type r: `numpy.ndarray`
  
    :param rho: Mass density.
    :type rho: `numpy.ndarray`
  
    :param menc: Enclosed mass. If not given, it'll be computed from density.
    :type menc: `numpy.ndarray, optional`

    :param G: Value for the gravitational constant. If not provided, CGS value is assumed.
    :type G: `float, optional`
  
    :return: Binding energy.
    :rtype: `float`
    """

  # Get constants (for G)
    if G == None: G = rytools.units.get_cgs()['G']

  # Get mass enclosed as a function of radius, if needed
    if menc == None: menc = rytools.stars.enclosed_mass_profile(r, rho)

    ebind_outer = - 4 * np.pi * G * np.array([scipy.integrate.simpson(menc[i:] * rho[i:] * r[i:], x = r[i:]) for i in range(len(r))])

    return ebind_outer

def post_main_sequence_core_idx(rho = None, x_h1 = None, criterion = 'hydrogen_mass_fraction'):

    """Determines the core boundary according to one of several criteria.
  
    :param rho: Mass density.
    :type rho: `numpy.ndarray, optional`

    :param x_h1: Hydrogen mass fraction.
    :type x_h1: `numpy.ndarray, optional`
  
    :param criterion: Criterion to use to determine the core boundary. Possible values are 'hydrogen_mass_fraction' (first cell with hydrogen mass fraction >= 0.1).
    :type criterion: `numpy.ndarray, optional`
  
    :return: Index of the first cell in the envelope.
    :rtype: `int`
    """

    if criterion == 'hydrogen_mass_fraction':
      assert type(x_h1) == np.ndarray, "Criterion is hydrogen mass fraction but no corresponding array was given!"
      idx = np.where(x_h1 >= 0.1)[0][0]

    return idx
