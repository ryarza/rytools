# Copyright 2022 Ricardo Yarza

# This file is part of rytools.

# rytools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# rytools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with rytools.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np

def get_cgs(source = None):

    """Fundamental and astrophysical constants according to the source specified. To be removed once I figure out which units package to use.
  
    :param source: Source from which to take constants. If None, a default set of constants is returned. If MESA, constants from MESA-r15140 are returned.
    :type source: `str`
  
    :return: Dictionary with constants and their values in CGS.
    :rtype: `dict`
    """

    cgs = {}

    # CODATA 2018
    cgs['G'] = 6.6743e-8
    cgs['GNEWT'] = cgs['G']

    # IAU 2012
    cgs['AU'] = 1.495978707e13

    # IAU 2015 conventions
    cgs['RSUN'] = 6.957e10
    cgs['LSUN' ] = 3.828e33
    cgs['GMSUN' ] = 1.3271244e26
    cgs['MSUN'] = cgs['GMSUN'] / cgs['G']
    cgs['RSUN' ] = 6.957e10

    # IAU recommended values for non-SI constants
    cgs['YEAR'] = 31557600

    # By definition
    cgs['CL'] = 2.99792458e10

    if source == None:
        cgs['QE']      = 4.80320680e-10
        cgs['ME']      = 9.1093826e-28
        cgs['MP']      = 1.67262171e-24
        cgs['MN']      = 1.67492728e-24
        cgs['HPL']     = 6.6260693e-27
        cgs['HBAR']    = 1.0545717e-27
        cgs['KBOL']    = 1.3806505e-16
        cgs['SIG']     = 5.670400e-5
        cgs['AR']      = 7.5657e-15
        cgs['THOMSON'] = 0.665245873e-24
        cgs['JY']      = 1.e-23
        cgs['PC']      = 3.085678e18

        cgs['REARTH'] = 6.3781e8
        cgs['MEARTH'] = 3.986004e20 / 6.6743e-8

        cgs['MJUP'] = 1.899e30
        cgs['RJUP'] = 7.149e9

        cgs['RY']      = 2.1798723611e-11
        cgs['ALPHA']   = cgs['QE'] * cgs['QE'] / cgs['HBAR'] / cgs['CL']
        cgs['RE']      = cgs['QE'] * cgs['QE'] / cgs['ME'] / cgs['CL'] / cgs['CL']
        cgs['ETA']     = np.exp( - np.euler_gamma )
    elif source == 'MESA':
        cgs['MJUP' ]  = 1.2668653e23 * cgs['MSUN'] / 1.3271244e26
        cgs['RJUP' ] = 7.1492e9
    else:
        raise ValueError("Source for constants not recognized.")


    return cgs
