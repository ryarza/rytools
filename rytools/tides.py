# Copyright 2022 Ricardo Yarza

# This file is part of rytools.

# rytools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# rytools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with rytools.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import scipy.interpolate

def T(n, l, eta):

  assert ( l <= 5 and l >= 1 ), "l value not supported"
  assert l == int(l), 'l must be an integer'
  l = int(l)
  assert ( n == 1.5 or n == 1), "n = " + str(n) + " not supported"
  if int(n) == n: n = int(n)

  dataDir = os.path.dirname(__file__) + "/data/tides/"
  etaGrid = np.loadtxt(dataDir + 'eta.txt')
  tGrid = np.loadtxt(dataDir + 'T_'+str(n)+'.dat')

  f = scipy.interpolate.interp1d(etaGrid, tGrid[:, l-1], kind = 'quadratic')
  return f(eta)

def DEoElin(n, q, x):
  eta = np.sqrt( q * pow(x, 3) / ( q + 1 ) )
  DEoE = np.empty(5)
  for l in range(1, 6):
    DEoE[l-1] = - (7/3) * pow(q, (2/3) * (2-l)) * pow(x, -2 * (1 + l )) * T(n, l, eta)

  return DEoE
