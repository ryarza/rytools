# Copyright 2022 Ricardo Yarza

# This file is part of rytools.

# rytools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# rytools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with rytools.  If not, see <https://www.gnu.org/licenses/>.

import rytools.units
import rytools.stars
import rytools.planets
import numpy as np
import scipy
import scipy.integrate

def delta_eorb(r, menc, mobj, G = None):

    """Change in energy of a circular orbit as a function of position along profile.

    :param r: Radial coordinate values at which other inputs are specified.
    :type r: `numpy.ndarray`

    :param menc: Enclosed mass.
    :type menc: `numpy.ndarray`

    :param mobj: Companion mass
    :type mobj: `float`

    :param G: Value for the gravitational constant. If not provided, CGS value is assumed.
    :type G: `float, optional`
  
    :return: Chage in energy.
    :rtype: `numpy.ndarray`
    """

  # Get constants (for G)
    if G == None: G = rytools.units.get_cgs()['G']

    eorb_0 = - G * mp * menc[-1] / 2 / r[-1]
    eorb = - G * mp * menc / 2 / r

    return eorb - eorb_0

def envelope_ejection_idxs(r, rho, mobj, menc = None, G = None):

    """Indices along profile in which envelope ejection is possible according to the alpha formalism.
  
    :param r: Radial coordinate values at which other inputs are specified.
    :type r: `numpy.ndarray`
  
    :param rho: Mass density.
    :type rho: `numpy.ndarray`
  
    :param mobj: Mass of the engulfed object.
    :type mobj: `float`
  
    :param menc: Enclosed mass. If not given, it'll be computed from density.
    :type menc: `numpy.ndarray, optional`

    :param G: Value for the gravitational constant. If not provided, CGS value is assumed.
    :type G: `float, optional`
  
    :return: Array of integer indices.
    :rtype: `numpy.ndarray`
    """

    if menc == None: menc = rytools.stars.enclosed_mass(r, rho)
    if G == None: G = rytools.units.get_cgs()['G']

    eorb0 = - G * menc[-1] * mobj / 2 / r[-1]
    eorbf = - G * menc     * mobj / 2 / r
    delta_eorb = eorbf - eorb0

    ebind_above = np.empty_like(delta_eorb)
    second_term = np.empty_like(delta_eorb)

    for k in range(len(ebind_above)):
      ebind_above[k] = rytools.stars.binding_energy(r = r, rho = rho, menc = menc, idx0 = k, idx1 = -1, G = G)
      second_term[k] = 4 * np.pi * cgs['G'] * mobj * scipy.integrate.simpson(y = r[k:] * rho[k:], x = r[k:])

    idxs = np.where(delta_eorb - second_term < ebind_above)[0]

    return idxs

def eject_envelope_before_destroyed(r, rho, x_h1, mobj, robj, menc = None, G = None):

    """Whether an engulfed object can eject the envelope before being destroyed, according to the alpha formalism.
  
    :param r: Radial coordinate values at which other inputs are specified.
    :type r: `numpy.ndarray`
  
    :param rho: Mass density.
    :type rho: `numpy.ndarray`
  
    :param mobj: Mass of the engulfed object.
    :type mobj: `float`
  
    :param menc: Enclosed mass. If not given, it'll be computed from density.
    :type menc: `numpy.ndarray, optional`

    :param G: Value for the gravitational constant. If not provided, CGS value is assumed.
    :type G: `float, optional`
  
    :return: Tuple containing (i) the deepest survived index, (ii) whether envelope ejection was possible then.
    :rtype: `(int, bool)`
    """

    if menc == None: menc = rytools.stars.enclosed_mass(r, rho)
    if G == None: G = rytools.units.get_cgs()['G']

  # Index at which engulfed object is destroyed as a result of either tides, ram pressure, or merging with the core
    tides_idx     = rytools.planets.tidal_disruption_idx(r = r, rho = rho, mobj = mobj, robj = robj, menc = menc)
    pressure_idx  = rytools.planets.ram_pressure_disruption_idx(r, rho, mobj, robj, menc = menc)
    core_idx      = rytools.stars.post_main_sequence_core_idx(rho = rho, x_h1 = x_h1, criterion = 'hydrogen_mass_fraction')
    destroyed_idx = int(np.amax(np.array([tides_idx, core_idx, pressure_idx])))

  # Indices at which the engulfed object has deposited enough orbital energy to overcome the binding energy of the envelope
    ejection_idxs = envelope_ejection_idxs(r, rho, mobj, menc = menc, G = G)
    if len(ejection_idxs) == 0: return destroyed_idx, False

  # Innermost ejection coordinate before being destroyed
    if not (ejection_idxs > destroyed_idx).any(): return destroyed_idx, False
    deepest_ejection_survived_idx = np.amin(ejection_idxs[ejection_idxs > destroyed_idx])

  # Criterion: if 90% of the envelope mass is ejected, we consider the statement "envelope was ejected successfully" as true
    mass_we_can_eject = menc[-1] - menc[deepest_ejection_survived_idx]
    envelope_mass = menc[-1] - menc[core_idx]

    if mass_we_can_eject / envelope_mass > 0.9: return deepest_ejection_survived_idx, True
    else: return destroyed_idx, False

def roche_limit(q):

    """Radial coordinate of the Roche limit.
  
    :param q: Mass ratio of accretor to donor mass.
    :type q: `array_like`
  
    :return: Radial coordinate of the Roche limit in units of original donor radius. Same datatype as q.
    :rtype: `array_like`
    """

    return (60/49) + (100/49) * np.power(q, 2/3) * np.log(1 + np.power(q, -1/3))

def average_gamma_loss(m_donor, m_accretor, gamma_ad, gamma_struct, f_corot):

    """Mass-loss-weighted average of specific angular momentum lost from a binary during the onset of mass transfer.
  
    :param m_donor: Donor mass in any units.
    :type m_donor: `array_like`
  
    :param m_accretor: Accretor mass in any units.
    :type m_accretor: `array_like`
  
    :param gamma_ad: Gas ratio of heat capacities.
    :type gamma_ad: `array_like`
  
    :param gamma_struct: Structural ratio of heat capacities.
    :type gamma_struct: `array_like`
  
    :param f_corot: Factor for corotation (see MacLeod & Loeb 2019)
    :type f_corot: `array_like`
  
    :return: Mass-loss-weighted average, with the same datatype as m_donor and m_accretor.
    :rtype: `numpy.ndarray or scalar`
    """

    q = m_accretor / m_donor
    gamma_d = q
    m_tot = m_donor + m_accretor
    gamma_l2 = np.power(1.2 * m_tot, 2) / m_donor / m_accretor
    return gamma_d + ( gamma_l2 - gamma_d ) * 0.66 * np.power(q/0.1, 0.08) * np.power(gamma_ad / (5/3), 0.69) * np.power(gamma_struct / (5/3), -2.17) * ( 1 - 0.3 * ( f_corot - 1 ) )

def mass_loss_ce_onset(m_donor, m_accretor, gamma_ad, gamma_struct, f_corot):

    """Amount of mass transfered for a binary as it shrinks from the Roche limit radius to the original donor radius.
  
    :param m_donor: Donor mass in any units.
    :type m_donor: `array_like`
  
    :param m_accretor: Accretor mass in any units.
    :type m_accretor: `array_like`
  
    :param gamma_ad: Gas ratio of heat capacities.
    :type gamma_ad: `array_like`
  
    :param gamma_struct: Structural ratio of heat capacities.
    :type gamma_struct: `array_like`
  
    :param f_corot: Factor for corotation (see MacLeod & Loeb 2019)
    :type f_corot: `array_like`
  
    :return: Mass transfered in the same units and with the same type as m_donor and m_accretor.
    :rtype: `numpy.ndarray or scalar`
    """

    gamma_loss_av = average_gamma_loss(m_donor, m_accretor, gamma_ad, gamma_struct, f_corot)
    roche_over_Rdonor = roche_limit(m_accretor / m_donor)
    return ( m_donor + m_accretor ) * np.log(np.sqrt(roche_over_Rdonor)) / gamma_loss_av

def L_emergent(times, edot, t_kh):

    L = np.empty_like(times)
    for idx_t, t, in enumerate(times):
        mask1 = t - times <= t_kh
        mask2 = times <= t
#        cells = np.where(val_t - t <= t_kh)[0]
#        cells = cells[cells <= idx_t]
        mask = mask1 & mask2

        edot_temp = np.zeros_like(edot)
        edot_temp[mask] = edot[mask]
#        edot_temp[edot_temp > 0] = 0

#        L[idx_t] = - scipy.integrate.trapz(edot_temp / t_kh, x = times)
        L[idx_t] = - scipy.integrate.simpson(edot_temp / t_kh, x = times)

    return L

def L_aftermath(t, edot, t_kh):
    cells = np.where(t + t_kh > t[-1])[0]
#    print("Final time: %.2e" % t[-1])
#    print("Expiration times:", (t + t_kh)[cells])
    addtl_times = np.sort( (t+t_kh)[cells])
    L_after = np.empty_like(addtl_times)

    assert np.amin(addtl_times) > np.amax(t)

    for idx_t, val_t in enumerate(addtl_times):
        cells = np.where(val_t - t < t_kh)[0]

        edot_temp = np.zeros_like(edot)
        edot_temp[cells] = edot[cells]


        L_after[idx_t] = - scipy.integrate.simpson(edot_temp / t_kh, x = t)

    return addtl_times, L_after

def tau_insp_grav(menc, msb, a, rho, h_rho):
    cgs = rytools.units.get_cgs()
    vorb = np.sqrt(cgs['G'] * menc / a)
    Ra = 2 * cgs['G'] * msb / pow(vorb, 2)
    F = rho * pow(vorb, 2) * np.pi * pow(Ra, 2)
    Edot = F * vorb
    E = - cgs['G'] * menc * msb / 2 / a
    vr = - ( Edot / E ) * a * ( 1 - 4 * np.pi * pow(a, 3) * rho / menc )
    return h_rho / vr
#    return ( menc / msb ) * vorb / ( 8 * np.pi * cgs['G'] * a * rho )
