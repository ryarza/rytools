# Copyright 2022 Ricardo Yarza

# This file is part of rytools.

# rytools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# rytools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with rytools.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import rytools.stars
import rytools.ce
import matplotlib.pyplot as plt
import scipy.interpolate
import scipy.integrate
import rytools.units

def mass_radius_relation():

    """Returns a function that gives radius as a function of mass between 1 Earth mass and 100 Jupiter masses. Input must be in Jupiter masses, and output will be in Jupiter radii.
  
    :return: A scipy interpolating function.
    :rtype: `scipy.interpolate.interpolate.interp1d`
    """
  
    import os
    data = np.loadtxt(os.path.dirname(__file__) + "/data/planets/mass-radius.txt")
  
    x = data[:,0]
    y = data[:, 1]
  
    cgs = rytools.units.get_cgs()
  
    def m_of_r(m):
      def point(m_val):
        if m_val <= 0.42:
          return ( cgs['REARTH'] / cgs['RJUP'] ) * pow(cgs['MJUP'] / cgs['MEARTH'], 1/2) * pow(m_val, 1/2)
        elif m_val < 1:
          m0 = 0.42
          m1 = 1
          dm = m1 - m0
          r0 = m_of_r(m0)
          r1 = m_of_r(m1)
          dr = r1 - r0
          return r0 + ( dr / dm ) * ( m_val - m0 )
  
        else:
          return scipy.interpolate.interp1d(x, y, kind='quadratic', bounds_error = True, fill_value=np.nan)(m_val)
  
      if type(m) == np.ndarray:
        if (m >= 1).all():
          return scipy.interpolate.interp1d(x, y, kind='quadratic', bounds_error = True, fill_value=np.nan)(m)
        else:
          r = np.empty(len(m))
          for idx, m_val in enumerate(m):
            r[idx] = point(m_val)
          return r
      else: return point(m)
  
    return m_of_r

def tidal_disruption_idx(r, rho, mobj, robj, menc = None):

    """Computes the indices in a stellar profile where the given engulfed object will be tidally disrupted.
  
    :param r: Radial coordinate values at which other inputs are specified.
    :type r: `numpy.ndarray`
  
    :param rho: Mass density.
    :type rho: `numpy.ndarray`
  
    :param mobj: Mass of the engulfed object.
    :type mobj: `float`
  
    :param robj: Radius of the engulfed object.
    :type robj: `float`
  
    :param menc: Enclosed mass. If not given, it'll be computed from density.
    :type menc: `numpy.ndarray, optional`
  
    :return: Index of outermost cell that satisfies r < tidal radius.
    :rtype: `float`
    """

    assert (np.diff(r) > 0 ).all()
 
    if menc == None: menc = rytools.stars.enclosed_mass_profile(r, rho)
 
    rho_enc_avg = np.empty_like(menc)
    rho_enc_avg[0] = rho[0]
  
    rho_enc_avg[1:] = menc[1:] / np.power(r[1:], 3)
    rho_SB_avg = mobj / np.power(robj, 3)
  #  rt = robj * np.power(menc / mobj, 1 / 3)
  
    return np.where(rho_enc_avg > rho_SB_avg)[0][-1]

def ram_pressure_disruption_idx(r, rho, mobj, robj, menc = None):

    """Computes the indices in a stellar profile where the given engulfed object will be disrupted by ram pressure according to the Jia+ 2018 criterion.
  
    :param r: Radial coordinate values at which other inputs are specified.
    :type r: `numpy.ndarray`
  
    :param rho: Mass density.
    :type rho: `numpy.ndarray`
  
    :param mobj: Mass of the engulfed object.
    :type mobj: `float`
  
    :param robj: Radius of the engulfed object.
    :type robj: `float`
  
    :param menc: Enclosed mass. If not given, it'll be computed from density.
    :type menc: `numpy.ndarray, optional`
  
    :return: Index of outermost cell that satisfies r < tidal radius.
    :rtype: `float`
    """
  
    if menc == None: menc = rytools.stars.enclosed_mass_profile(r, rho)

    V_SB = 4 * np.pi * pow(robj, 3) / 3
    rho_SB_avg = mobj / V_SB
    f = ( menc / mobj ) * ( rho / rho_SB_avg )
  
    return np.where(f > 1)[0][-1]

def decay_luminosity_exceeds_stellar_luminosity(r, dr, t, t_thermal, v_conv, edot, Lstar):

    L_decay_max = np.amax(L_emergent(t, edot, t_kh))

    return L_decay_max > Lstar

def decay_luminosity(Msb, Mst, Rsb, Rst, rho, Cd = 1, G = None):

    """Energy deposition rate onto the stellar envelope during inspiral.
  
    :param Msb: Mass of the engulfed object.
    :type Msb: `float`
  
    :param Mst: Mass of the star.
    :type Mst: `float`
  
    :param Rsb: Radius of the engulfed object.
    :type Rsb: `float`
  
    :param Rst: Radius of the star.
    :type Rst: `float`
  
    :param Msb: Envelope mass density.
    :type Msb: `float`
  
    :return: Energy deposition rate onto the stellar envelope during inspiral.
    :rtype: `float`

    :param G: Value for the gravitational constant. If not provided, CGS value is assumed.
    :type G: `float, optional`

    """
  
    if G == None: G = rytools.units.get_cgs()['G']
    vkep = np.sqrt( G * ( Mst + Msb ) / Rst )
    return Cd * np.pi * pow(Rsb, 2) * rho * pow(vkep, 3)

def decay_luminosity_morgan(Msb, Mst, Rsb, Rst, Cd = 1, eta = 0.1, G = None):

    """Energy deposition rate onto the stellar envelope during inspiral.
  
    :param Msb: Mass of the engulfed object.
    :type Msb: `float`
  
    :param Mst: Mass of the star.
    :type Mst: `float`
  
    :param Rsb: Radius of the engulfed object.
    :type Rsb: `float`
  
    :param Rst: Radius of the star.
    :type Rst: `float`
  
    :param Cd: Drag coefficient.
    :type Cd: `float, optional`
  
    :param eta: Factor by which density differs from the average stellar density.
    :type eta: `float, optional`

    :param G: Value for the gravitational constant. If not provided, CGS value is assumed.
    :type G: `float, optional`
  
    :return: Energy deposition rate onto the stellar envelope during inspiral.
    :rtype: `float`
  
    """
  
    if G == None: G = rytools.units.get_cgs()['G']
    vkep = np.sqrt( G * ( Mst + Msb ) / Rst )
    Vst = 4 * np.pi * pow(Rst, 3) / 3
    rho = eta * Mst / Vst
  
    return Cd * np.pi * pow(Rsb, 2) * rho * pow(vkep, 3)

def load_orbint_output(output_path, scalars_path, profile_path = None, sample = False, nsamples = 300, sample_type = 'linear', compute_derived = False, average = False):

    data = rytools.read_file_with_headers(output_path)
    scalars = rytools.read_file_with_headers(scalars_path)
    cgs = rytools.units.get_cgs()

    d = {}
    for key in data.keys():
        d[key] = data[key].copy()
    d['r']        *= scalars['l_unit']
    d['time']     *= scalars['t_unit']
    d['rdot']     *= scalars['l_unit'] / scalars['t_unit']
    d['thetadot'] /= scalars['t_unit']
    d['menc']     *= scalars['m_unit']
    d['rho']      *= scalars['m_unit'] / pow(scalars['l_unit'], 3)
    d['f_grav']   = d['f_grav'] * scalars['m_unit'] * scalars['l_unit'] / pow(scalars['t_unit'], 2)
    d['f_ram']    *= scalars['m_unit'] * scalars['l_unit'] / pow(scalars['t_unit'], 2)

    if average:
        d['period_circular'] = np.sqrt(pow(d['r'], 3) / cgs['G'] / d['menc'])
        d_new = {}
        for key in d:
            d_new[key] = d[key].copy()
            for idx in range(len(d_new[key])):
                if d['r'][idx] / cgs['RSUN'] < 100:
                    idx0, t0 = rytools.nearest(d['time'], d['time'][idx] - 20. * d['period_circular'][idx])
    #                assert idx0 > 0
                    idx1, t1 = rytools.nearest(d['time'], d['time'][idx] + 20. * d['period_circular'][idx])
    #                assert idx1 < len(d['time'] - 1)
                    if key == 'time':
                        d_new['time'][idx] = d['time'][idx]#( t0 + t1 ) / 2
                    else:
                        d_new[key][idx] = scipy.integrate.simpson(d[key][idx0:idx1+1], d['time'][idx0:idx1+1]) / ( d['time'][idx1] - d['time'][idx0] )
                else:
                    d_new[key][idx] = d[key][idx]
        d = d_new            

    
    d['msb'] = scalars['Msb'] * scalars['m_unit']
    d['rsb'] = scalars['Rsb'] * scalars['l_unit']

    assert ( np.diff(d['time']) > 0 ).all()

    # Stellar profile
    import os
    if profile_path == None:
        profile_path = os.path.relpath(os.path.join(os.path.dirname(output_path), scalars['profile_path'].replace('h5', 'data')))
        print("  Loading corresponding MESA profile %s" % profile_path)
    d['prof'] = rytools.stars.load_mesa_profile(profile_path)

    if sample:

        # Determine the destruction index to make sure we get that!
        d_temp = d.copy()
        d_temp['vtheta'] = d_temp['r'] * d_temp['thetadot']
        d_temp['vtot'] = np.sqrt( pow(d_temp['rdot'], 2) + pow(d_temp['vtheta'], 2) )
        d_temp['vesc_SB'] = np.sqrt(cgs['G'] * d_temp['msb'] / d_temp['rsb'] )
        d_temp['av_rho_SB'] = d_temp['msb'] / ( 4 * np.pi * pow(d_temp['rsb'], 3) / 3 )
        d_temp['av_rho_enc'] = d_temp['menc'] / ( 4 * np.pi * pow(d_temp['r'], 3) / 3 )
        d_temp['destroyed'] = {}
        d_temp['destroyed']['tidal_disruption'] = d_temp['av_rho_enc'] > d_temp['av_rho_SB']
        d_temp['destroyed']['evaporation'] = False
        d_temp['destroyed']['ram_disruption'] = d_temp['rho'] * pow(d_temp['vtot'], 2) > d_temp['av_rho_SB'] * pow(d_temp['vesc_SB'], 2)
        d_temp['destroyed']['total'] = d_temp['destroyed']['tidal_disruption'] | d_temp['destroyed']['evaporation'] | d_temp['destroyed']['ram_disruption']
        last_survived_idx = np.where(d_temp['destroyed']['total'] == True)[0][0] - 1

        d['itp'] = {}
        for key in d.keys():
            if key != 'time' and key != 'itp' and key != 'prof' and key != 'msb' and key != 'rsb':
                d['itp'][key] = scipy.interpolate.interp1d(d['time'], d[key], kind = 'linear')

        

        new_time_1 = np.linspace(0, d['time'][last_survived_idx], nsamples)
        n2 = int( 2 - nsamples  + ( nsamples - 1 ) * d['time'][-1] / d['time'][last_survived_idx] ) + 1
        # At least two points (requirement for some assertions below)
        assert n2 >= 2
        new_time_2 = np.linspace(d['time'][last_survived_idx], d['time'][-1], n2)[1:]
        new_time = np.concatenate((new_time_1, new_time_2))
        # No repeated times
        assert ( np.unique(new_time) == new_time ).all()
        # New time contains last survived time
        assert d['time'][last_survived_idx] in new_time
        # Last times match
        assert new_time[-1] == d['time'][-1], "%.5e %.5e" % ( new_time[-1], d['time'][-1] )
        # Time is monotonically increasing
        assert ( np.diff(new_time) > 0).all()

        for key in d.keys():
            if key != 'time' and key != 'itp' and key != 'prof' and key != 'msb' and key != 'rsb':
                d[key] = d['itp'][key](new_time)

        d['time'] = new_time

    ## Compute derived quantities
    # Coordinates
    d['x'] = d['r'] * np.cos(d['theta'])
    d['y'] = d['r'] * np.sin(d['theta'])
    # SB properties
    d['av_rho_SB'] = d['msb'] / ( 4 * np.pi * pow(d['rsb'], 3) / 3 )
    d['vesc_SB'] = np.sqrt(cgs['G'] * d['msb'] / d['rsb'] )
    # Speed
    d['vtheta'] = d['r'] * d['thetadot']
    d['rp_over_ra'] = d['rsb'] / ( 2 * cgs['G'] * d['msb'] / pow(d['vtheta'], 2) )
    d['vtot'] = np.sqrt( pow(d['rdot'], 2) + pow(d['vtheta'], 2) )
    d['mach_theta'] = d['vtheta'] / d['prof']['itp']['cs'](d['r'])
    # Timescales
    d['tau_insp_grav_analytical'] = rytools.ce.tau_insp_grav(d['menc'][0], d['msb'], d['r'][0], d['rho'][0], d['prof']['itp']['h_rho'](d['r'][0]))
    d['t_kh'] = d['prof']['itp']['t_kh'](d['r'])
    # Orbital energies
    d['epot_specific'] = - cgs['G'] * d['menc'] / d['r']
    d['eorb'] = 0.5 * d['msb'] * pow(d['vtot'], 2) + d['msb'] * d['epot_specific']
    d['delta_eorb'] = d['eorb'] - d['eorb'][0]
    d['dedt'] = np.gradient(d['eorb'], d['time'], edge_order = 2)
    d['eorb_circular'] = - cgs['G'] * d['msb'] * d['menc'] / 2 / d['r']
    d['f_theta'] = d['f_ram'] + d['f_grav']
    d['edot_from_forces'] = {}
    d['edot_from_forces']['total'] = d['vtheta'] * d['f_theta']
    d['edot_from_forces']['grav'] = d['vtheta'] * d['f_grav']
    d['edot_from_forces']['ram'] = d['vtheta'] * d['f_ram']
    # Accretion
    d['ra'] = 2 * cgs['G'] * d['msb'] / pow(d['vtheta'], 2)
    d['rp_over_ra'] = d['rsb'] / d['ra']
    d['sigma_a'] = np.pi * pow(d['ra'], 2)
    d['mdot_hl'] = d['rho'] * d['vtot'] * np.pi * d['sigma_a']
    d['delta_m_hl'] = scipy.integrate.simpson(d['mdot_hl'], d['time'])
    # Destruction criteria
    d['av_rho_enc'] = d['menc'] / ( 4 * np.pi * pow(d['r'], 3) / 3 )
    d['destroyed'] = {}
    d['destroyed']['tidal_disruption'] = d['av_rho_enc'] > d['av_rho_SB']
#    d['destroyed']['evaporation'] = d['cs'] > d['vesc_SB']
    d['destroyed']['evaporation'] = False
    d['destroyed']['ram_disruption'] = d['rho'] * pow(d['vtot'], 2) > d['av_rho_SB'] * pow(d['vesc_SB'], 2)
    d['destroyed']['total'] = d['destroyed']['tidal_disruption'] | d['destroyed']['evaporation'] | d['destroyed']['ram_disruption']
    d['survived'] = np.invert(d['destroyed']['total'])

#    assert ( (d['survived'] == False).any() or d['r'][-1] < d['rsb']), "Integration stopped before destruction point!"

    if type(compute_derived) == dict:
        if compute_derived['deltae_from_forces']:
            d['deltae_from_forces'] = rytools.integral_array(d['time'], d['f_theta'] * d['vtheta'])
        if compute_derived['L_emergent']:
            d['L_emergent'] = rytools.ce.L_emergent(d['time'], d['edot_from_forces']['total'], d['t_kh'])
            if ( d['survived'] == True ).any(): d['t_aftermath'], d['L_aftermath'] = rytools.ce.L_aftermath(d['time'][d['survived']], d['edot_from_forces']['total'][d['survived']], d['t_kh'][d['survived']])
            else:
                d['t_aftermath'] = np.array([0])
                d['L_aftermath'] = np.array([0])

            d['L_combined'] = {}
            d['L_combined']['t'] = np.concatenate( ( d['time'], d['t_aftermath'] ) )
            d['L_combined']['L'] = np.concatenate( ( d['L_emergent'], d['L_aftermath'] ) )
            d['t_for_which_L_exceeds_Lstar'] = time_range_where_L_exceeds_Lstar(d['L_combined']['t'], d['L_combined']['L'], d['prof']['L'][-1])

    return d

def time_range_where_L_exceeds_Lstar(t, L, Lstar):

    mask = L > Lstar
#    print("%.2e %.2e" % ( t[mask][-1], t[mask][0] ))
    return t[mask][-1] - t[mask][0]
