# Copyright 2022 Ricardo Yarza

# This file is part of rytools.

# rytools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# rytools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with rytools.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
#import rytools.bremss
#import rytools.cholla
#import rytools.grmonty
#import rytools.multitidal
import scipy
import scipy.integrate
import rytools.plot
import rytools.stars
import rytools.polytropes
import rytools.units
import rytools.planets

def h5_to_dict(h5_file_path):
    import h5py
    f = h5py.File(h5_file_path, 'r')
    d = {}
    for key in f.keys():
        d[key] = np.array(f[key][:])
    f.close()
    return d

def integral_array(x, y):
    integral = np.empty_like(x)
    integral[0] = 0.
    for idx in range(1, len(integral)):
        integral[idx] = scipy.integrate.simpson(y[:idx+1], x[:idx+1])
    return integral

def nearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return idx, array[idx]

def read_file_with_headers_numpy(path):
  data = {}
  values = np.loadtxt(path, skiprows=1)
  with open(path) as f:
    keys = f.readline().rstrip()
    keys = keys.split()

  if '#' in keys: keys.remove('#')

  for idx, key in enumerate(keys):
    if values.ndim == 1:
      data[key] = values[idx]
    else:
      data[key] = values[:,idx]

  return data

def read_file_with_headers(path):
    data = {}
    with open(path, 'r') as file:
        raw_data = [line.strip().split() for line in file.readlines()]
    raw_data = np.array(raw_data)
    for idx, key in enumerate(raw_data[0]):
        data[key] = raw_data[1:, idx]
        # Try to convert data to float. Some data aren't float though!
        try: data[key] = np.array(data[key], dtype = 'float')
        except: pass
        # Convert to integer if applicable
        try:
            if ( data[key] == np.array(data[key], dtype = int)).all():
                data[key] = np.array(data[key], dtype = int)
        except: pass
        # Convert to scalar if applicable
        if len(data[key]) == 1:
            data[key] = data[key][0]

    return data

def apply_mask_to_dict(dictionary, mask):
  filtered_dict = {}
  for key in dictionary.keys():
    filtered_dict[key] = dictionary[key][mask]
  return filtered_dict

def get_consecutive_ranges(array):

  """Splits the input array into arrays of consecutive elements, and keeps only the first and last values of the spli
t arrays.

  :param array: Input array.
  :type array: `numpy.ndarray`

  :return: A list of numpy arrays. Each numpy array is either 1 or 2 elements long.
  :rtype: `scipy.interpolate.interpolate.interp1d`
  """

  lst = np.split(array, np.where(np.diff(array) != 1)[0] + 1)
  for idx, array in enumerate(lst):
    if len(array) > 1: lst[idx] = np.array([ array[0], array[-1] ])
  return lst

def replace_in_file(path, search_exp, new_line):
  import fileinput
  import sys

  """Replaces lines that match a search expression in a file.

  :param path: Path to file (replace is done in-place).
  :type path: `str`

  :param search_exp: Expression with which lines will be matched.
  :type search_exp: `str`

  :param new_line: Lines that contain the search expression will be replaced by this new line.
  :type new_line: `str`

  :return: 0 if successful.
  :rtype: `int`
  """

  for line in fileinput.input(path, inplace = 1):
    if search_exp in line:
      line = new_line + '\n'
    sys.stdout.write(line)
  return 0

def get_slab_idxs(npoints, nranks, rank):

  """Returns the minimum and maximum indices that must be passed to an array of length npoints to get the rank-th piece if the array were split into nranks pieces."""

  idxs = np.empty(2, dtype = int)
  idxs[0] = int(rank * npoints / nranks)
  idxs[1] = int(( rank + 1 ) * npoints / nranks - 1)
  return idxs
