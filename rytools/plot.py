# Copyright 2022 Ricardo Yarza

# This file is part of rytools.

# rytools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# rytools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with rytools.  If not, see <https://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import subprocess
import os

def plottex(fontsize = 'xx-large'):

  try:
    subprocess.call(['rm', "%s/tex.cache/*" % matplotlib.get_cachedir()], stdout = subprocess.DEVNULL, stderr = subprocess.STDOUT)
  except:
    print("Couldn't remove matplotlib TeX cache!")

  plt.rc('text', usetex=True)
  plt.rc('font', family='serif')
  plt.rc('xtick', labelsize=fontsize)
  plt.rc('ytick', labelsize=fontsize)
  plt.rc('axes', labelsize=fontsize)
  plt.rc('legend', fontsize=fontsize)

  this_file_path = os.path.dirname(os.path.abspath(__file__))
  defs_path = os.path.abspath(os.path.join(this_file_path, 'data/tex/defs.tex'))
  params= {'text.latex.preamble' : r'\usepackage{amsmath} \usepackage[print-unity-mantissa=false]{siunitx} \input{%s}' % defs_path}
  plt.rcParams.update(params)

def axUnitLabel(symbol, unit):
  return r'$'+str(symbol)+r'$ $\ls'+str(unit)+r'\rs$'

def pointsToPcolormeshArgs(x, y, z):

  dx = x[1] - x[0]
  dy = y[1] - y[0]

  x = np.append(x, x[-1] + dx)
  y = np.append(y, y[-1] + dy)

  x -= dx / 2
  y -= dy / 2

  xx = np.zeros((len(y), len(x)))
  yy = np.zeros((len(y), len(x)))

  for j in range(0, len(y)):
    xx[j] = x

  for i in range(0, len(x)):
    yy[:, i] = y

  return xx, yy, z.T
