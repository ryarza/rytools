# Copyright 2022 Ricardo Yarza

# This file is part of rytools.

# rytools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# rytools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with rytools.  If not, see <https://www.gnu.org/licenses/>.

def load_cg(path, requested_integration_radius, time_av_low = 20):

    # Radii at which the common envelope setup computed the force integral
    radii = np.genfromtxt(path + 'df_sum_radii.dat')[1:, 1]
    # Values of the force at these radii
    grav_force = rytools.read_file_with_headers_numpy(path + 'acc.dat')

    # Make sure there are no NaNs
    assert not (np.isnan(radii)).any(), "%s contains NaNs!" % path
    # Make sure radii are strictly increasing
    assert (np.diff(radii) > 0).all()
    # Make sure the simulation ran long enough to be time-averaged
    assert (np.diff(grav_force['#time']) > 0).all(), "Time not increasing after around %.2e" % grav_force['#time'][np.where(np.diff(grav_force['#time']) < 0)[0]]

    # Choose the closest available radii to the one requested
    idx, integration_radius = rytools.nearest(radii, requested_integration_radius)
    if not np.isclose(integration_radius, requested_integration_radius, rtol = 1.e-1):
        print("Warning: requested radius %.2e Ra but closest was %.2e Ra" % ( requested_integration_radius, integration_radius ) )

    # Compute drag coefficients from time-averaged forces.
    mask = grav_force['#time'] > time_av_low
    times = grav_force['#time'][mask]
    forces = grav_force['df_x_%i' % ( idx + 1 )][mask]
    return scipy.integrate.simpson(forces, times) / ( times[-1] - times[0] ) / np.pi

def load_cp(path, rp_over_ra, time_av_low = 20):
    # Load pressure force
    pressure_force = rytools.read_file_with_headers_numpy(path)
    assert rp_over_ra > 0
    assert ( np.diff(pressure_force['time']) > 0).all(), "Time must be strictly increasing"
    assert time_av_low < pressure_force['time'][-1], "Requested initial averaging time is after final simulation time"

    # Compute drag coefficients from time-averaged forces
    mask = pressure_force['time'] > time_av_low
    times = pressure_force['time'][mask]
    forces = pressure_force['pressure_force_x'][mask]
    return scipy.integrate.simpson(forces, times) / ( times[-1] - times[0] ) / np.pi / pow(rp_over_ra, 2)
