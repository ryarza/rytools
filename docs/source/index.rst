.. rytools documentation master file, created by
   sphinx-quickstart on Fri Jul  9 15:42:33 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to rytools's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ce  
   planets
   stars
   units



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
