# Copyright 2022 Ricardo Yarza

# This file is part of rytools.

# rytools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# rytools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with rytools.  If not, see <https://www.gnu.org/licenses/>.

import rytools.ce
import pytest
import numpy as np

def test_mass_loss_ce_onset():
# Reproduce table 1 from MacLeod & Loeb 2020. Their table is actual simulation results so just demand relative difference ~ 0.3
  m2           = np.array([0.1, 0.1 , 0.1 , 0.1, 0.01, 0.03, 0.3, 0.1 , 0.1 , 0.1 , 0.3 , 0.3 , 0.3 , 0.1])
  m1           = np.ones_like(m2)
  a_i          = np.array([1.73, 1.73, 1.73, 1.73, 1.2, 1.51, 2.06, 1.55, 1.55, 1.55, 1.75, 1.75, 1.75, 1.55])
  a_f = np.ones_like(a_i)
  gamma_ad     = np.array([5/3, 5/3 , 5/3 , 5/3, 5/3 , 5/3 , 5/3, 1.35, 1.5 , 5/3 , 1.35, 1.5 , 5/3 , 5/3])
  gamma_struct = np.array([5/3, 5/3 , 5/3 , 5/3, 5/3 , 5/3 , 5/3, 1.35, 1.35, 1.35, 1.35, 1.35, 1.35, 5/3])
  f_corot      = np.array([1  , 0.67, 0.33, 0  , 0   , 1   , 1  , 1   , 1   , 1   , 1   , 1   , 1   , 1  ])

  sim_results = np.array([25, 23, 22, 21, 1.3, 9.2, 86, 16, 14, 13, 53, 45, 46, 22]) * 1e-3

  assert rytools.ce.mass_loss_ce_onset(m1, m2, gamma_ad, gamma_struct, f_corot) == pytest.approx(sim_results, rel = 1)
