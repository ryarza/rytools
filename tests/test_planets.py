# Copyright 2022 Ricardo Yarza

# This file is part of rytools.

# rytools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# rytools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with rytools.  If not, see <https://www.gnu.org/licenses/>.

import rytools.planets
import numpy as np
import rytools.units
import rytools.polytropes
import pytest

def test_mass_radius_relation():
  r_of_m = rytools.planets.mass_radius_relation()
  x_vals = np.linspace(1.01, 100, 100)
  y_vals = r_of_m(x_vals)

# Make sure key features of the relation are there
# Local maximum at 5 MJup
  local_max = np.amax(y_vals[x_vals<6])
  local_max_x = x_vals[np.argmax(y_vals[x_vals<6])]
  assert local_max > 1.09 and local_max < 1.1

# Local minimum at 60 MJup
  local_min = np.amin(y_vals)
  local_min_x = x_vals[np.argmin(y_vals)]
  assert local_min > 0.9 and local_min < 0.91
  assert local_min_x > 58 and local_min_x < 59

# Negative derivative from local maximum to local minimum
  dy_dx = np.gradient(x_vals, y_vals, edge_order = 2)
  assert (dy_dx[(x_vals >= 1.2 * local_max_x) & (x_vals <= 0.8 * local_min_x)] < 0).all()

# Positive derivative after that
  assert (dy_dx[(x_vals >= 1.2 * local_min_x)] > 0).all()

  return 0

def test_disruption_idx():
# An n = 1 polytrope
  xi = np.linspace(0, 4, 201)
  theta = 1 - xi * xi / 6
  theta_avg = 1 - xi * xi / 10
  assert rytools.planets.tidal_disruption_idx(r = xi, rho = theta, mobj = 0.6 * 4 * np.pi / 3, robj = 1, menc = None) == 99
