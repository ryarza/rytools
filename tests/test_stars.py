# Copyright 2022 Ricardo Yarza

# This file is part of rytools.

# rytools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# rytools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with rytools.  If not, see <https://www.gnu.org/licenses/>.

import rytools
import numpy as np
import pytest

cgs = rytools.units.get_cgs()

def test_binding_energy():

# Integrate polytropic profile for the sun
  poly = rytools.polytropes.DimensionalPolytrope(1, cgs['MSUN'], cgs['RSUN'])
  r = np.linspace(0, cgs['RSUN'], 100)
  rho = poly.rho(r)

  assert rytools.stars.binding_energy(r, rho) == pytest.approx(poly.binding_energy, rel = 1e-6)

# Integrate a profile for which we analytically know the binding energy between two radii
  r = np.linspace(0, 1, 101)
  rho = 1 - r
# Get binding energy between r = 0.3 and r = 0.8
  assert rytools.stars.binding_energy(r, rho, idx0 = 30, idx1 = 80) / cgs['GNEWT'] == pytest.approx(- 945001 * np.pi * np.pi / 15750000)
# Edge case from 0.5 to 1
  assert rytools.stars.binding_energy(r, rho, idx0 = 50, idx1 = -1) / cgs['GNEWT'] == pytest.approx(- 29 * np.pi * np.pi / 420)

  return 0

def test_enclosed_mass():

# Uniform sphere - full sphere
  r = np.linspace(0, 1, 101)
  rho = np.ones_like(r)
  assert rytools.stars.enclosed_mass_profile(r, rho)[-1] == pytest.approx(4 * np.pi / 3)

# Polytrope
  poly = rytools.polytropes.DimensionalPolytrope(1, cgs['MSUN'], cgs['RSUN'])
  r = np.linspace(0, cgs['RSUN'], 100)
  rho = poly.rho(r)

  assert rytools.stars.enclosed_mass_profile(r, rho)[-1] == pytest.approx(poly.m)

  return 0

def test_post_main_sequence_core_idx():
# A fake stellar profile
  x_h1 = np.array([0., 0., 0., 0., 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6])
  assert rytools.stars.post_main_sequence_core_idx(x_h1 = x_h1, criterion = 'hydrogen_mass_fraction') == 5
  return 0
