# Copyright 2022 Ricardo Yarza

# This file is part of rytools.

# rytools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# rytools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with rytools.  If not, see <https://www.gnu.org/licenses/>.

import rytools
import numpy as np

def test_nearest():

# Array for which nearest matches exactly
  x = np.linspace(0, 1, 101)
  assert rytools.nearest(x, 0.5) == ( 50, 0.5)
  assert rytools.nearest(x, 1  ) == (100, 1  )
  assert rytools.nearest(x, 0  ) == (  0, 0  )

# Array for which it doesn't
  x = np.linspace(0, 1, 5)
  assert rytools.nearest(x, 0.3) == (  1, 1/4)

  return 0

def test_slab():

  """Splits an array, puts it back together, and makes sure it matches the original."""

  nranks = 10
  npoints = 100
  original_array = np.linspace(0, 1, npoints)
  reconstructed_array = np.array([])
  for rank in range(nranks):
    idxs = rytools.get_slab_idxs(npoints, nranks, rank)
    array_local = original_array[idxs[0]:idxs[1] + 1]
    assert len(array_local) == int(npoints / nranks), "Local array has incorrect length"
    reconstructed_array = np.append(reconstructed_array, array_local)

  assert (original_array == reconstructed_array).all()

def test_get_consecutive_ranges():

  """Compares with calculation by hand."""

  array = np.array([0, 2, 3, 4, 6, 9], dtype = int)
  ranges = rytools.get_consecutive_ranges(array)
  assert (ranges[0] == np.array([0])).all()
  assert (ranges[1] == np.array([2, 4])).all()
  assert (ranges[2] == np.array([6])).all()
  assert (ranges[3] == np.array([9])).all()

  return 0
